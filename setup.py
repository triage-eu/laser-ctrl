from setuptools import find_packages
from setuptools import setup


setup(
    name='laser_ctrl',
    use_scm_version=True,
    package_dir={'': 'src'},
    packages=find_packages('src'),
    version='1.0.0',
    description='Python library for controlling Norblis Laser',
    install_requires=[
        'pyserial',
        'tqdm',
    ],
    maintainer='Guzmán Borque Gallego',
    maintainer_email='gbg@csem.ch',
    python_requires='>=3',
    license='MIT'
)
