# Laser Control

Python package to control Norblis Laser for TRIAGE.

## Installation

The `laser_ctrl` package can be installed with `pip` after cloning the repository.

```shell
git clone https://gitlab.csem.local/div-e/project/triage/laser-ctrl.git
cd laser-ctrl

# Optional but strongly recommended.
virtualenv venv
source venv/bin/activate  # or venv\Scripts\activate on Windows.

pip install --editable .
```

## Usage

The package provides a command-line interface to control basic laser functionality.
