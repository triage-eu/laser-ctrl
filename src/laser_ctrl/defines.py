'''
    Defines all constant values to communicate with Connet laser.
'''

# Fixed messages
GET_BASIC_INFO = [0xAA, 0x55, 0xD1, 0x00, 0x00, 0x00]
GET_DEFINITION_OF_WORKING_STATUS = [0xAA, 0x55, 0xD2, 0x00, 0x00, 0x00]
GET_WORKING_STATUS_INFORMATION = [0xAA, 0x55, 0xD3, 0x00, 0x00, 0x00]
GET_RULES_FOR_SETTING_PULSE_PARAMETER = [0xAA, 0x55, 0xD4, 0x00, 0x00, 0x00]
SET_AS_DEFAULT_SETTINGS = [0xAA, 0x55, 0xA1, 0x00, 0x00, 0x00]
RESTORE_FACTORY_SETTINGS = [0xAA, 0x55, 0xC2, 0x00, 0x00, 0x00]
SET_LASER_POWER_ON = [0xAA, 0x55, 0xC1, 0x01, 0x01, 0x01, 0x00]
SET_LASER_POWER_OFF = [0xAA, 0x55, 0xC1, 0x01, 0x00, 0x00, 0x00]
GET_PUMP_POWER_OF_BOOST_STAGE = [0xAA, 0x55, 0xC3, 0x00, 0x00, 0x00]
SET_PUMP_POWER_OF_BOOST_STAGE = [0xAA, 0x55, 0xC3, 0x02, 0x00, 0x00, 0x00,
                                 0x00]
GET_TEMPERATURE_OF_SEED_LASER = [0xAA, 0x55, 0xC7, 0x00, 0x00, 0x00]
GET_REPETITION_RATE = [0xAA, 0x55, 0xCA, 0x00, 0x00, 0x00]
SET_REPETITION_RATE = [0xAA, 0x55, 0xCA, 0x02, 0x00, 0x00, 0x00, 0x00]
GET_PULSE_WIDTH = [0xAA, 0x55, 0xC9, 0x00, 0x00, 0x00]
SET_PULSE_WIDTH = [0xAA, 0x55, 0xC9, 0x02, 0x00, 0x00, 0x00, 0x00]
GET_REPETITION_RATE_AND_PULSE_WIDTH = [0xAA, 0x55, 0xCD, 0x00, 0x00, 0x00]
#for SN 202103147
#SET_REPETITION_RATE_AND_PULSE_WIDTH = [0xAA, 0x55, 0xCD, 0x04, 0xB8, 0x0B,
#                                       0x05, 0x00, 0xC8, 0x00]
#for SN 202103146
# below 0B 0A is the swapped bytes to set rep rate to 3MHz, 0A 00 is the swapped
# bytes to set the pulse width to 1ns which is sent with coefficient of 10
# in your case and the CD 00 is the swapped parity
SET_REPETITION_RATE_AND_PULSE_WIDTH = [0xAA, 0x55, 0xCD, 0x04, 0xB8, 0x0B,
                                       0x0A, 0x00, 0xCD, 0x00]
SET_TRIGGER_TO_INTERNAL = [0xAA, 0x55, 0xCE, 0x01, 0x00, 0x00, 0x00]
SET_TRIGGER_TO_EXTERNAL = [0xAA, 0x55, 0xCE, 0x01, 0x01, 0x01, 0x00]
GET_TRIGGER_MODE = [0xAA, 0x55, 0xCE, 0x00, 0x00, 0x00]

ALARM_LASER_TEMP = 0
ALARM_SEED_TEMP = 1
ALARM_INPUT_POWER = 2
ALARM_PUMP_TEMP = 3
ALARM_PUMP_POWER = 4
ALARM_SEED_POWER = 5
ALARM_SAVE_DATA = 7

ERROR_CMD_LOWER_THAN_MIN = 0
ERROR_CMD_HIGHER_THAN_MAX = 1
ERROR_CMD_BLOCKED_STATUS = 2
ERROR_CMD_PROD_CANT_EXEC = 3

# Associate bytes to strings (Connet manual)
POWER_UNIT_DICT = {0: "mW", 1: "W", 2: "uW"}
PULSE_UNIT_DICT = {0: "ns", 1: "us", 2: "ps"}
REP_UNIT_DICT = {0: "Hz", 1: "kHz", 2: "MHz"}
TRIGGER_MODE_DICT = {0: "Internal trigger", 1: "External trigger"}
POWER_MODE_DICT = {1: "Set output power", 2: "Set current", 3: "Set DA",
                   4: "Potentiometer", 5: "Fix power", 6: "Set voltage",
                   7: "Set pump power", 8: "Set peak power", 9: "Set gain",
                   10: "Set monitor power"}
WORKING_STATUS_DICT = {0x10: "Pump Temperature",   0x11: "Pump 1 Temperature",
                       0x12: "Pump 2 Temperature", 0x1F: "Module Temperature",
                       0x1D: "Seed Temperature",   0x20: "Pump Current",
                       0x21: "Pump 1 Current",     0x22: "Pump 2 Current",
                       0x50: "Output Power",       0x5E: "Pump Power",
                       0x31: "Pump 1 Pin Value"}
PRODUCT_TYPE_DICT = {2: "Pulsed laser without amplification",
                     3: "Pulsed laser with amplification"}
STATUS_DICT = {0: "OFF", 1: "ON"}
POWER_ON_TIME = 2*60  # 2 mins of warm up and cool down time
BAUD_RATE = 9600

N_BYTES_MAX = 100  # Read up to one hundred bytes
