'''
    Defines auxiliary functions.
'''
import platform


def get_usb():
    '''
        Returns VID/PID to connect to laser.
    '''
    os_type = platform.system()
    pid = ''
    if os_type == 'Linux':
        pid = 'USB VID:PID=0403:6001'  # Change to correct value
    elif os_type == 'Windows':
        pid = 'USB VID:PID=0403:6001'
    return pid


def byte_print(b_in):
    '''
        Auxiliary function to print bytes in hexadecimal form.
    '''
    return f"b'{''.join(f'|x{b:02X}' for b in b_in)}'"


def raw_to_temp(raw, coeff):
    '''
        Auxiliary function that transforms raw reading to temperature.
    '''
    return (float(raw) - 27315) / coeff


def raw_to_power(raw, coeff):
    '''
        Auxiliary function that transforms raw reading to power.
    '''
    return float(raw) / coeff
