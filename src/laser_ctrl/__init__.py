'''
    Defines LaserCtrl class for communicating and controlling Norblis laser.
'''

from time import time
from math import floor
import serial
from serial.tools.list_ports import comports
from tqdm import tqdm  # Progress bar

from laser_ctrl import defines  # Constant definitions
from laser_ctrl import utils    # Aux functions


class LaserCtrl():
    '''
        Class LaserCtrl to manipulate Norblis Laser.
    '''

    # ---------------------------------------------------------------------------------
    # Private Methods
    # ---------------------------------------------------------------------------------
    def _send(self, command, init_n):  # init_n: end of message header
        self._serial.write(command)
        response = self._serial.read(defines.N_BYTES_MAX)
        # print(f'Response: {byte_print(response)}')
        length = len(response)
        # print(response[2],'==',command[2],', ',length,'==',response[3] + 6)
        if response[2] == command[2] and length == response[3] + 6:
            parity_bytes = 2
            response_data = response[init_n+1:length-parity_bytes]
        else:
            response_data = bytes(0)
        return response_data

    def _create_power_command(self, power=0):
        power_bytes = power.to_bytes(2, 'little')
        parity = power_bytes[0] + power_bytes[1]
        parity_bytes = parity.to_bytes(2, 'little')
        # Keep header only and add power and parity bytes
        command = defines.SET_PUMP_POWER_OF_BOOST_STAGE[0:4]
        command.append(power_bytes[0])
        command.append(power_bytes[1])
        command.append(parity_bytes[0])
        command.append(parity_bytes[1])

        return command

    def _verify_set_command(self, rec_msg):
        error_code = rec_msg[0]  # Only one byte (convert to int)
        if error_code & (1 << defines.ERROR_CMD_LOWER_THAN_MIN) != 0:
            print('Error executing command: Value being set lower than min. '
                  'Set to minimum value.')
        if error_code & (1 << defines.ERROR_CMD_HIGHER_THAN_MAX) != 0:
            print('Error executing command: Value being set greater than max. '
                  'Set to maximum value.')
        if error_code & (1 << defines.ERROR_CMD_BLOCKED_STATUS) != 0:
            print('Error executing command: This command cannot be executed '
                  'in current status.')
        if error_code & (1 << defines.ERROR_CMD_PROD_CANT_EXEC) != 0:
            print('Error executing command: This product does not have this '
                  'functionality.')
        return error_code

    def _set_exact_power(self, power):
        command = self._create_power_command(power)
        response = self._send(command, 3)
        return self._verify_set_command(response)

    def _generate_ramp(self, initial_power, target_power, pb=False):
        previous_power = initial_power
        initial_time = time()
        sign = (target_power-initial_power) / abs(target_power-initial_power)
        slope = self._power_slope * sign
        if pb:
            pbar = tqdm(total=defines.POWER_ON_TIME)
            step_time = 5  # s
            previous_time = initial_time
        while sign*self.power < target_power:
            current_time = time()
            if pb:
                if current_time - previous_time > step_time:
                    pbar.update(step_time)
                    previous_time = current_time
            power = int(slope * (current_time - initial_time) + initial_power)
            power = int(floor(power / self._power_incr) * self._power_incr)
            if power == previous_power:  # skip if no step increase
                continue
            previous_power = power
            if sign*power >= target_power:
                power = target_power
            if self._set_exact_power(power):
                print(f'Error setting power '
                      f'{utils.raw_to_power(power, self._power_coef)} '
                      f'{defines.POWER_UNIT_DICT[self._power_unit]}')
                break
        if pb:
            pbar.close()

    def _get_basic_info(self):
        # Get basic information
        response = self._send(defines.GET_BASIC_INFO, 11)
        # Get Serial Number
        (ini, fin) = (0, 9)
        self._serial_number = 0
        for i in range(ini, fin):
            self._serial_number += response[i] * 10**(fin-1-i)
        # Get maximum pump power
        (ini, fin) = (10, 12)
        self._power_unit = response[ini-1]
        if self._serial_number == 202103147:
            self._target_power = int.from_bytes(response[ini:fin], 'little')
            self._power_incr = 10
            self._power_decr = 10
        elif self._serial_number == 202103146:
            self._target_power = 4250 #mW
            self._power_incr = 10
            self._power_decr = 10
        else:
            print("WARNING: new laser is detected. Power parameters must be added in /home/triage/.local/lib/python3.8/site-packages/laser_ctrl/__init__.py")
            self._target_power = 100
            self._power_incr = 10
            self._power_decr = 10
        self._power_slope = self._target_power / defines.POWER_ON_TIME
        # Get power setting mode
        self._power_mode = response[fin]
        # Get temperature coefficient
        (ini, fin) = (13, 14)
        self._temp_coef = response[ini]
        # Get power coefficient
        (ini, fin) = (15, 16)
        self._power_coef = response[ini]
        # Get product type
        (ini, fin) = (23, 24)
        self._product_type = response[ini]
        # Get seed temperature range
        (ini, mid, fin) = (24, 26, 28)
        self._temp_min = utils.raw_to_temp(
                int.from_bytes(response[ini:mid], 'little'), self._temp_coef)
        self._temp_max = utils.raw_to_temp(
                int.from_bytes(response[mid:fin], 'little'), self._temp_coef)

    def _get_pulse_info(self):
        response = self._send(defines.GET_RULES_FOR_SETTING_PULSE_PARAMETER, 3)
        # Get pulse width info
        self._pulse_unit = response[0]
        self._pulse_coef = response[1]
        self._pulse_min = int.from_bytes(response[2:4], 'little')
        self._pulse_max = int.from_bytes(response[4:6], 'little')
        self._pulse_step = response[6]
        # Get repetition rate info
        self._rep_unit = response[7]
        self._rep_coef = response[8]
        self._rep_min = int.from_bytes(response[9:11], 'little')
        self._rep_max = int.from_bytes(response[11:13], 'little')
        self._rep_step = response[13]

    def _print_laser_info(self):
        print(' ')
        print('----------------------------------------------------')
        print('            Norblis Laser Information')
        print('----------------------------------------------------')
        print(f'Serial number: {self._serial_number}')
        print(f'Pump max power: {self._target_power} '
              f'{defines.POWER_UNIT_DICT[self._power_unit]}')
        print(f'Pump power increment steps: {self._power_incr} '
              f'{defines.POWER_UNIT_DICT[self._power_unit]}')
        print(f'Power mode: {self._power_mode} '
              f'({defines.POWER_MODE_DICT[self._power_mode]})')
        print(f'Temperature Coefficient: {self._temp_coef}')
        print(f'Power Coefficient: {self._power_coef}')
        print(f'Product type: {self._product_type} '
              f'({defines.PRODUCT_TYPE_DICT[self._product_type]})')
        print(f'Seed temperature range: {self._temp_min}-{self._temp_max} °C')
        print(f'Pump max power: {self._target_power} '
              f'{defines.POWER_UNIT_DICT[self._power_unit]}')
        print(f'Pulse width range: {self._pulse_min/self._pulse_coef}-'
              f'{self._pulse_max/self._pulse_coef} '
              f'{defines.PULSE_UNIT_DICT[self._pulse_unit]}')
        print(f'Pulse width step: {self._pulse_step/self._pulse_coef} '
              f'{defines.PULSE_UNIT_DICT[self._pulse_unit]}')
        print(f'Repetition rate range: {self._rep_min/self._rep_coef}-'
              f'{self._rep_max/self._rep_coef} '
              f'{defines.REP_UNIT_DICT[self._rep_unit]}')
        print(f'Repetition rate step: {self._rep_step/self._rep_coef} '
              f'{defines.REP_UNIT_DICT[self._rep_unit]}')

    def _parse_working_info(self):
        self._parsed_working_info = []
        self._parsed_working_info_def = []
        for i, definition in enumerate(self._working_info_def):
            # [i:i+1] only returns [i]
            value_u16 = int.from_bytes(self._working_info[2*i:2*i+2], 'little')
            value_type = definition >> 4 & 0xF
            if value_type == 1:  # Temperature reading
                value = utils.raw_to_temp(value_u16, self._temp_coef)
                units = '°C'
            elif value_type == 5:  # Power reading
                value = int(utils.raw_to_power(value_u16, self._power_coef))
                units = defines.POWER_UNIT_DICT[self._power_unit]
            else:  # Other (current)
                value = value_u16
                units = 'mA'
            self._parsed_working_info.append(value)
            self._parsed_working_info_def.append(
                defines.WORKING_STATUS_DICT[definition] + ' [' + units + ']')

    def _get_working_info_def(self):
        # Get Working info definition first
        working_info_def = self._send(
                defines.GET_DEFINITION_OF_WORKING_STATUS, 4)
        self._working_info_def = working_info_def

    def _get_working_info(self):
        # Get Definition first and then actual info
        if self._working_info_def is None:
            self._get_working_info_def()
        working_info = self._send(
                defines.GET_WORKING_STATUS_INFORMATION, 3)
        # Get laser status (ON/OFF)
        self._status = working_info[0]
        # Get alarm codes
        self._alarm = working_info[1]
        if self._alarm & (1 << defines.ALARM_LASER_TEMP) != 0:
            print('Alarm: Laser temperature abnormal')
        if self._alarm & (1 << defines.ALARM_SEED_TEMP) != 0:
            print('Alarm: Seed temperature abnormal')
        if self._alarm & (1 << defines.ALARM_INPUT_POWER) != 0:
            print('Alarm: Input power abnormal')
        if self._alarm & (1 << defines.ALARM_PUMP_TEMP) != 0:
            print('Alarm: Pump temperature abnormal')
        if self._alarm & (1 << defines.ALARM_PUMP_POWER) != 0:
            print('Alarm: Pump power abnormal')
        if self._alarm & (1 << defines.ALARM_SEED_POWER) != 0:
            print('Alarm: Seed power abnormal')
        if self._alarm & (1 << defines.ALARM_SAVE_DATA) != 0:
            print('Alarm: Save data error')
        self._working_info = working_info[2:len(working_info)]
        self._parse_working_info()

    def _print_working_info(self):
        print(' ')
        print('----------------------------------------------------')
        print('            Norblis Laser Working Info')
        print('----------------------------------------------------')
        print(f'Laser status: {self._status} '
              f'({defines.STATUS_DICT[self._status]})')
        for i, definition in enumerate(self._parsed_working_info_def):
            print(f'{definition}: {self._parsed_working_info[i]}')

    def _turn_on(self):
        '''
        Turn laser on
        '''
        if self.is_open:
            self._get_working_info()
            if self._alarm:
                print('Laser alarm is active. Cannot power on laser')
                return
            if self.status:
                print('Laser already ON')
                return

            # Turn Laser ON
            response = self._send(defines.SET_LASER_POWER_ON, 3)
            if self._verify_set_command(response):
                print('Error setting laser ON/OFF')
                return
        else:
            print('Cannot turn on. No laser communication is open')

    def _turn_off(self):
        '''
        Turn laser off
        '''
        if self.is_open:
            if not self.status:
                print('Laser already OFF')
                return
            if self.power > 0:
                print('Cannot turn off laser, power > 0 mW')
                return
            # Turn Laser ON
            response = self._send(defines.SET_LASER_POWER_OFF, 3)
            if self._verify_set_command(response):
                print('Error setting laser OFF')
                return
        else:
            print('Cannot turn off. No laser communication is open')

    def __init__(self, pid=None):
        # Correct values are assigned below through properties
        self._serial = None
        # Basic info
        self._serial_number = None
        self._target_power = None
        self._power_incr = None
        self._power_decr = None
        self._power_unit = None
        self._power_mode = None
        self._power_slope = None
        self._temp_coef = None
        self._power_coef = None
        self._product_type = None
        self._temp_min = None
        self._temp_max = None
        # Pulse+Repetition info
        self._pulse_unit = None
        self._pulse_coef = None
        self._pulse_min = None
        self._pulse_max = None
        self._pulse_step = None
        self._rep_unit = None
        self._rep_coef = None
        self._rep_min = None
        self._rep_max = None
        self._rep_step = None
        # Working status
        self._status = None
        self._alarm = None
        self._working_info_def = None
        self._working_info = None

        self._power = None
        self._temp = None
        self._trigger_mode = None
        self._pulse_width = None
        self._rep_rate = None

        self._thread = None

        self._port_list = list(comports())

    def __del__(self):
        del self._serial

    # ---------------------------------------------------------------------------------
    # Public Methods
    # ---------------------------------------------------------------------------------
    def connect(self, pid=None):
        if self._serial is None:
            # First connection
            if pid is None:
                pid = utils.get_usb()
            com_ports_list = list(comports())
            laser_port = None
            for port in com_ports_list:
                if port[2].startswith(pid):
                    laser_port = port[0]  # Laser found by VID/PID match.
                    break  # stop searching-- we are done.
            if laser_port is None:
                print("No Norblis laser found")
                return -1
            srl = serial.Serial(port=laser_port, baudrate=defines.BAUD_RATE,
                                timeout=0.1, parity=serial.PARITY_NONE,
                                stopbits=serial.STOPBITS_ONE,
                                bytesize=serial.EIGHTBITS)
            self._serial = srl

            # Initialise laser info (basic + pulse width + repetition rate)
            self._get_basic_info()
            self._get_pulse_info()
            self._print_laser_info()
            print("Connected to Norblis laser")
        else:
            # Reopen connection serial object exists
            if self.is_open:
                print("Already connected to Norblis laser")
            else:
                self._serial.open()
                print("Connected to Norblis laser")
        return 0

    def disconnect(self):
        if self.is_open:
            self._serial.close()
            print("Disconnected from Norblis laser")
        else:
            print("Already disconnected from Norblis laser")

    def set_as_default(self):
        '''
        Set current settings as default
        '''
        if self.is_open:
            print('Setting current laser config as default...')
            self._get_basic_info()
            self._get_pulse_info()
            self._print_laser_info()
            response = self._send(defines.SET_AS_DEFAULT_SETTINGS, 3)
            if self._verify_set_command(response):
                print('Error setting laser config as default.')
                return
            print('DONE')
        else:
            print('No laser communication is open')

    def restore_factory_settings(self):
        '''
        Restore factory settings
        '''
        if self.is_open:
            print('Restoring factory settings...')
            response = self._send(defines.RESTORE_FACTORY_SETTINGS, 3)
            if self._verify_set_command(response):
                print('Error restoring factory settings.')
                return
            self._get_basic_info()
            self._get_pulse_info()
            self._print_laser_info()
            print('DONE')
        else:
            print('No laser communication is open')

    def set_trigger_mode(self):
        '''
        Set current settings as default
        '''
        if self.is_open:
            print('Setting current laser config as default...')
            self._get_basic_info()
            self._get_pulse_info()
            self._print_laser_info()
            response = self._send(defines.SET_AS_DEFAULT_SETTINGS, 3)
            if self._verify_set_command(response):
                print('Error setting laser config as default.')
                return
            print('DONE')
        else:
            print('No laser communication is open')

    def update_working_info(self):
        '''
        Get laser working information (status, currents, temperature)
        '''
        if self.is_open:
            self._get_working_info()
            self._print_working_info()
        else:
            print('No laser communication is open')

    def power_on(self):
        '''
        Turn on laser and generate power ramp to target_power mW
        '''
        if self.is_open:
            # Turn laser on
            self._turn_on()
            # Progressively increase power to avoid damage
            print(f'Powering laser ON... ETA = {defines.POWER_ON_TIME} s')
            self._generate_ramp(self.power, self._target_power, True)
            print(f'DONE. Current power: '
                  f'{int(utils.raw_to_power(self.power, self._power_coef))} '
                  f'{defines.POWER_UNIT_DICT[self._power_unit]}')
            self._get_working_info()
            self._print_working_info()
        else:
            print('No laser communication is open')

    def power_off(self):
        '''
        Turn off laser generating power ramp to 0 mW
        '''
        if self.is_open:
            self._get_working_info()
            if self._alarm:
                print('Laser alarm is active. Cannot power on laser')
                return
            if not self._status:
                print('Laser already OFF')
                return

            # Progressively decrease power to avoid damage
            initial_power = self.power
            if initial_power > 0:
                print(f'Powering laser OFF... ETA = {defines.POWER_ON_TIME} s')
                self._generate_ramp(initial_power, 0, True)
                print(f'DONE. Current power: '
                      f'{int(utils.raw_to_power(self.power,self._power_coef))}'
                      f' {defines.POWER_UNIT_DICT[self._power_unit]}')

            # Turn Laser OFF
            response = self._send(defines.SET_LASER_POWER_OFF, 3)
            if self._verify_set_command(response):
                print('Error setting laser ON/OFF')
                return
            self._get_working_info()
            self._print_working_info()
        else:
            print('No laser communication is open')

    @property
    def is_open(self):
        '''
        Get laser communication status (0: Closed, 1: Open)
        '''
        if self._serial is None:
            return 0
        return self._serial.is_open

    @property
    def status(self):
        '''
        Get laser status (ON/OFF)
        '''
        if self.is_open:
            response = self._send(defines.GET_WORKING_STATUS_INFORMATION, 3)
            self._status = response[0]
            # print(f'Laser status: {self._status} '
            #       f'({STATUS_DICT[self._status]})')
        else:
            print('No laser communication is open')
        return self._status

    # @status.setter
    # def status(self, status):
    #     '''
    #     Set laser status (0: OFF, 1: ON)
    #     '''
    #     if self.is_open:
    #         if status == 0:
    #             print('Turning laser OFF...')
    #             response = self._send(defines.SET_LASER_POWER_OFF, 3)
    #         elif status == 1:
    #             print('Turning laser ON...')
    #             response = self._send(defines.SET_LASER_POWER_ON, 3)
    #         else:
    #             print('Status must be 0 (OFF) or 1 (ON)')
    #             return
    #         if self._verify_set_command(response):
    #             print('Error setting laser ON/OFF')
    #             return
    #         self._status = status
    #         print('DONE')
    #     else:
    #         print('No laser communication is open')

    @property
    def power(self):
        '''
        Get laser power in mW
        '''
        if self.is_open:
            # Get current power
            response = self._send(defines.GET_PUMP_POWER_OF_BOOST_STAGE, 3)
            # [i:i+1] only returns [i]
            self._power = int.from_bytes(response[0:2], 'little')
            # print(f'Current power: '
            #       f'{int(raw_to_power(self._power, self._power_coef))} '
            #       f'{POWER_UNIT_DICT[self._power_unit]}')
        else:
            print('No laser communication is open')
        return self._power

    # @power.setter
    # def power(self, power):
    #     '''
    #     Set laser power in mW
    #     '''
    #     if self.is_open:
    #         print(f'Setting power: '
    #               f'{int(utils.raw_to_power(power, self._power_coef))} '
    #               f'{defines.POWER_UNIT_DICT[self._power_unit]}')
    #         initial_power = self.power
    #         if initial_power != power:
    #             self._generate_ramp(initial_power, power)
    #         print('DONE')
    #     else:
    #         print('No laser communication is open')

    @property
    def temp(self):
        '''
        Get laser temperature in °C
        '''
        if self.is_open:
            # Get current power
            response = self._send(defines.GET_TEMPERATURE_OF_SEED_LASER, 3)
            # [i:i+1] only returns [i]
            self._temp = int.from_bytes(response[0:2], 'little')
            # print(f'Current temperature: '
            #       f'{raw_to_temp(self._temp, self._temp_coef)}°C')
        else:
            print('No laser communication is open')
        return self._temp

    @property
    def rep_rate(self):
        '''
        Get laser repetition rate
        '''
        if self.is_open:
            response = self._send(defines.GET_REPETITION_RATE, 3)
            # [i:i+1] only returns [i]
            self._rep_rate = int.from_bytes(response[0:2], 'little')
            # print(f'Current repetition rate: '
            #       f'{self._rep_rate/self._rep_coef} '
            #       f'{REP_UNIT_DICT[self._rep_unit]}')
        else:
            print('No laser communication is open')
        return self._rep_rate

    # @rep_rate.setter
    # def rep_rate(self):
    #     '''
    #     Set laser repetition rate
    #     '''
    #     if self.is_open:
    #         response = self._send(defines.SET_REPETITION_RATE, 3)
    #         # [i:i+1] only returns [i]
    #         self._rep_rate = int.from_bytes(response[0:2], 'little')
    #         # print(f'Current repetition rate: '
    #         #       f'{self._rep_rate/self._rep_coef} '
    #         #       f'{REP_UNIT_DICT[self._rep_unit]}')
    #     else:
    #         print('No laser communication is open')
    #     return self._rep_rate

    @property
    def pulse_width(self):
        '''
        Get laser pulse width
        '''
        if self.is_open:
            response = self._send(defines.GET_PULSE_WIDTH, 3)
            # [i:i+1] only returns [i]
            self._pulse_width = int.from_bytes(response[0:2], 'little')
            # print(f'Current pulse width: '
            #       f'{self._pulse_width/self._pulse_coef} '
            #       f'{PULSE_UNIT_DICT[self._pulse_unit]}')
        else:
            print('No laser communication is open')
        return self._pulse_width

    @property
    def trigger_mode(self):
        '''
        Get laser trigger mode
        '''
        if self.is_open:
            response = self._send(defines.GET_TRIGGER_MODE, 3)
            self._trigger_mode = response[0]
            # print(f'Trigger mode: {self._trigger_mode} '
            #       f({TRIGGER_MODE_DICT[self._trigger_mode]})')
        else:
            print('No laser communication is open')
        return self._trigger_mode

    # @trigger_mode.setter
    # def trigger_mode(self, mode):
    #     '''
    #     Set laser trigger mode (0: internal, 1: external)
    #     '''
    #     if self.is_open:
    #         if mode == 0:
    #             response = self._send(defines.SET_TRIGGER_TO_INTERNAL, 3)
    #         elif mode == 1:
    #             response = self._send(defines.SET_TRIGGER_TO_EXTERNAL, 3)
    #         else:
    #             print('Trigger mode must be 0 (internal) or 1 (external)')
    #             return
    #         if self._verify_set_command(response):
    #             print('Error turning laser OFF')
    #             return
    #         self._trigger_mode = mode
    #     else:
    #         print('No laser communication is open')

    @property
    def port_list(self):
        '''
        Get port list from pyserial
        '''
        port_list = list(comports())
        port_list_str = []
        for port in port_list:
            port_list_str.append(port[0])
        self._port_list = port_list_str
        return self._port_list


if __name__ == '__main__':
    laser = LaserCtrl()
    laser.connect()

    print(f'Is communication open? {laser.is_open}')

    laser.update_working_info()
    laser.power_on()
    laser.power_off()
    laser.disconnect()
