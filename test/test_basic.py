import pytest
from laser_ctrl import LaserCtrl  # The code to test


@pytest.mark.parametrize('pid', [None, 'USB VID:PID=0403:6001'])
def test_connection(pid):
    laser = LaserCtrl(pid)
    laser.connect()
    assert laser.is_open is True


def test_get_info():
    laser = LaserCtrl()
    laser.update_working_info()
    assert laser == laser


@pytest.mark.parametrize('status', [1, 0])
def test_power_on_off(status):
    laser = LaserCtrl()
    laser.connect()
    laser.status = status
    assert status == laser.status


@pytest.mark.parametrize('power', [0, 10, 20])
def test_set_power(power):
    laser = LaserCtrl()
    laser.connect()
    laser.status = 1
    laser.power = power
    actual = laser.power
    laser.power = 0
    laser.status = 0
    assert actual == power
